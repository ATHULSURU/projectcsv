fetch('runs.json')
  .then(resp => resp.json())
  .then((data) => {
    const key = Object.keys(data);
    const value = Object.values(data);
    Highcharts.chart('container', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Total runs scored by team',
      },
      xAxis: {
        categories: key,
        title: {
          text: ' TEAMS ------> ',
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: ' TOTAL RUNS ------->',
        },
      },
      legend: {
        enabled: false,
      },
      series: [{
        name: 'total runs per team',
        data: value,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
