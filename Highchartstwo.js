fetch('rcbbatsman.json')
  .then(resp => resp.json())
  .then((data) => {
    const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const Totalruns = [];
    const Players = [];
    for (let i = 0; i < 10; i += 1) {
      Totalruns.push(keysSorted[i]);
      Players.push(data[keysSorted[i]]);
    }
    Highcharts.chart('container1', {
      chart: {
        type: 'bar',
      },
      title: {
        text: 'Top ten scored batsman in RCB',
        style: {
          fontFamily: 'Lucida Console',
        },
      },
      xAxis: {
        categories: Totalruns,
        title: {
          text: 'Players',
          style: {
            fontFamily: 'Verdana, sans-serif',
            color: 'black',
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: 'TOTAL RUNS ------>',
          style: {
            fontFamily: 'Verdana, sans-serif',
            color: 'black',
          },
        },
      },
      legend: {
        enabled: false,
      },
      series: [{
        name: 'individual runs',
        data: Players,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
