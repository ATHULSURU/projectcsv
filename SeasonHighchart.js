fetch('teamSeason.json')
  .then(resp => resp.json())
  .then((data) => {
    const array = [];
    const pba = Object.keys(data);
    for (let j = 0; j < pba.length; j += 1) {
      array.push({
        name: pba[j],
        data: Object.values(data[pba[j]]),
      });
    }

    Highcharts.chart('container3', {
      chart: {
        type: 'bar',
      },
      title: {
        text: 'Matches Played By Team By Season',
      },
      xAxis: {
        title: {
          text: 'Years',
        },
        categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017],
      },
      yAxis: {
        min: 0,
        title: {
          text: 'IPL teams',
        },
      },
      legend: {
        reversed: true,
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: array,
    });
  });
