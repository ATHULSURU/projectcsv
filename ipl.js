const csv = require('csv-parser');

const fs = require('fs');

const teams = {};

fs.createReadStream('deliveries.csv')

  .pipe(csv())
  .on('data', (line) => {
    const team = line.batting_team;
    const runs = parseInt(line.total_runs, 0);
    if (!teams[team]) {
      teams[team] = 0;
    }
    teams[team] += runs;
  })
  .on('end', () => {
    console.log(teams);
    fs.writeFile('runs.json', JSON.stringify(teams));
  });
