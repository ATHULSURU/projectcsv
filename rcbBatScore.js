const csv = require('csv-parser');
const fs = require('fs');

const players = {};

fs.createReadStream('deliveries.csv')

  .pipe(csv())
  .on('data', (line) => {
    const player = line.batsman;
    const runs = parseInt(line.batsman_runs, 0);

    if (line.batting_team === 'Royal Challengers Bangalore') {
      if (!players[player]) {
        players[player] = 0;
      }

      players[player] += runs;
    }
  })
  .on('end', () => {
    fs.writeFile('rcbbatsman.json', JSON.stringify(players));
  });
