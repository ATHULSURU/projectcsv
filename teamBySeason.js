const csvParse = require('csv-parser');
const fs = require('fs');


const seasons = {};
let y;
fs.createReadStream('matches.csv')
  .pipe(csvParse())
  .on('data', (line) => {
    const year = line.season;
    const team1 = line.team1;
    const team2 = line.team2;

    if (!seasons[team1]) {
      seasons[team1] = {};
    }
    if (!seasons[team2]) {
      seasons[team2] = {};
    }
    if (!seasons[team1][year] || !seasons[team2][year]) {
      seasons[team1][year] = 0;
      seasons[team2][year] = 0;
    }

    seasons[team1][year] += 1;
    seasons[team2][year] += 1;
  })
  .on('end', () => {
    const key = Object.keys(seasons);
    for (let i = 2008; i <= 2017; i += 1) {
      for (let j = 0; j < key.length; j += 1) {
        y = seasons[key[j]];
        if (!y[String(i)]) {
          y[String(i)] = 0;
        }
      }
    }
    console.log(seasons);
    fs.writeFile('teamSeason.json', JSON.stringify(seasons));
  });
