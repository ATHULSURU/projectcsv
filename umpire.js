fetch('umpire.json')
  .then(resp => resp.json())
  .then((data) => {
    const key = Object.keys(data);
    const value = Object.values(data);
    Highcharts.chart('container2', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Foreign umpire analysis',
      },
      xAxis: {
        categories: key,
        title: {
          text: ' Nationality ------> ',
          style: {
            fontFamily: 'Verdana, sans-serif',
            color: 'black',
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: ' Number of Umpires ------->',
          style: {
            fontFamily: 'Verdana, sans-serif',
            color: 'black',
          },
        },
      },
      legend: {
        enabled: false,
      },
      series: [{
        name: 'total runs per team',
        data: value,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
